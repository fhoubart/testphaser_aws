const webpack = require("webpack");
const path = require("path");

let config = {
    entry: "./src/index.js",
    output: {
      path: path.resolve(__dirname, "./public_html"),
      filename: "./bundle.js"
    },
    mode: "development",
    /*module: {
        rules: [{
          test: /\.js$/,
          exclude: /node_modules/,
          loader: "babel-loader"
        }]
      },*/
      devServer: {
        static: {
            directory: path.join(__dirname, 'public_html'),
          },
          compress: true,
          port: 9000
        }
  }
  
  module.exports = config;

